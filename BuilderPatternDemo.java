enum Size {
	Small, Medium, Large, XLarge
}
enum Cheese {
	Cheddar, Mozzarella, Parmesan, Provolone
}
class Pizza {
	private final Size size;
	private final Cheese cheese;
	private final Boolean mushrooms;
	private final Boolean peppers;
	private final Boolean ham; 
	private final Boolean pineapple;
	private final Boolean shallots;
	private final Boolean paprika;
	private final Boolean sauce;
	private final int slices;

	public Pizza(Size size,
				 Cheese cheese,
				 Boolean mushrooms,
				 Boolean peppers,
				 Boolean ham,
				 Boolean pineapple,
				 Boolean shallots,
				 Boolean paprika,
				 Boolean sauce,
				 int slices) {	
		this.size = size;
		this.cheese = cheese;
		this.mushrooms = mushrooms;
		this.peppers = peppers;
		this.ham = ham;
		this.pineapple = pineapple;
		this.shallots = shallots;
		this.paprika = paprika;
		this.sauce = sauce;
		this.slices = slices;
	}
	public Size getSize() {
		return size;
	}
	public Cheese getCheese() {
		return cheese;
	}
	public Boolean getMushrooms() {
		return mushrooms;
	}
	public Boolean getPepperoni() {
		return peppers;
	}
	public Boolean getHam() {
		return ham;		
	}
	public Boolean getPineapple() {
		return pineapple;
	}
	public Boolean getShallots() {
		return shallots;
	}
	public Boolean getPeppers() {
		return paprika;
	}
	public Boolean getSauce() {
		return sauce;
	}
	public int getSlices() {
		return slices;
	}
}
class PizzaBuilder {
	private Size size;
	private Cheese cheese;
	private Boolean mushrooms;
	private Boolean peppers;
	private Boolean ham; 
	private Boolean pineapple;
	private Boolean shallots;
	private Boolean paprika;
	private Boolean sauce;
	private int slices;

	public PizzaBuilder() {}

	public PizzaBuilder setSize(Size size) {
		this.size = size;
		return this;
	}
	public PizzaBuilder addCheese(Cheese cheese) {
		this.cheese = cheese;
		return this;
	}
	public PizzaBuilder addMushrooms() {
		mushrooms = true;
		return this;
	}
	public PizzaBuilder addPepperoni() {
		peppers = true;
		return this;
	}
	public PizzaBuilder addHam() {
		ham = true;
		return this;
	}
	public PizzaBuilder addPineapple() {
		pineapple = true;
		return this;
	}
	public PizzaBuilder addShallots() {
		shallots = true;
		return this;
	}
	public PizzaBuilder addPeppers() {
		paprika = true;
		return this;
	}
	public PizzaBuilder addSauce() {
		sauce = true;
		return this;
	}
	public PizzaBuilder setSlices(int slices) {
		this.slices = slices;
		return this;
	}
	public Pizza build() {
		return new Pizza(size, cheese, mushrooms, peppers, ham, pineapple, shallots, paprika, sauce, slices);
	}
}
class Director {
	public Pizza buildPepperoniPizza() {
		return new PizzaBuilder()
				.setSize(Size.XLarge)
				.addCheese(Cheese.Mozzarella)
				.addPepperoni()
				.addSauce()
				.setSlices(8)
				.build();
	}
	public Pizza buildHawaianPizza() {
		return new PizzaBuilder()
				.setSize(Size.Large)
				.addCheese(Cheese.Mozzarella)
				.addHam()
				.addPineapple()
				.addSauce()
				.setSlices(8)
				.build();
	}
	public Pizza buildCheddarLoverPizza() {
		return new PizzaBuilder()
				.setSize(Size.Large)
				.addCheese(Cheese.Cheddar)
				.addShallots()
				.addPeppers()
				.addSauce()
				.setSlices(4)
				.build();
	}
	public Pizza buildParmesanPerfectionPizza() {
		return new PizzaBuilder()
				.setSize(Size.Medium)
				.addCheese(Cheese.Parmesan)
				.addHam()
				.addShallots()
				.addSauce()
				.setSlices(2)
				.build();
	}
	public Pizza buildProvoloneSupremePizza() {
		return new PizzaBuilder()
				.setSize(Size.Small)
				.addCheese(Cheese.Provolone)
				.addMushrooms()
				.addPepperoni()
				.addSauce()
				.setSlices(1)
				.build();
	}
}
public class BuilderPatternDemo {
	public static void main(String[] args) {
		System.out.println("Without Builder:");
		withoutBuilder();
		System.out.println("\nWith Builder:");
		withBuilder();
		System.out.println("\nBy Director:");
		byDirectorClass();
	}
	public static void withoutBuilder() {
		Pizza p = new Pizza(Size.XLarge, null, true, null, null, null, true, true, null, 4);
		printDetailsPizza(p);
	}
	public static void withBuilder() {
		Pizza p = new PizzaBuilder()
				.setSize(Size.Large)
				.setSlices(8)
				.addCheese(Cheese.Mozzarella)
				.addSauce()
				.build();
		printDetailsPizza(p);
	}
	public static void byDirectorClass() {
		Pizza p = new Director().buildPepperoniPizza();
		printDetailsPizza(p);
	}
	public static void printDetailsPizza(Pizza p) {
		StringBuilder ingredients = new StringBuilder("");

		ingredients.append(p.getMushrooms() != null ? "mushrooms, " : "");
		ingredients.append(p.getShallots() != null ? "shallots, " : "");
		ingredients.append(p.getPeppers() != null ? "peppers, " : "");
		ingredients.append(p.getCheese() != null ? p.getCheese() + " cheese, " : "");
		ingredients.append(p.getPepperoni() != null ? "pepperoni, " : "");
		ingredients.append(p.getSauce() != null ? "sauce, " : "");
		ingredients.delete(ingredients.length() - 2, ingredients.length());
		ingredients.append(".");

		System.out.println("Successfully to make a " +  p.getSize() + " pizza with " + p.getSlices() + " slices.");
		System.out.println("Ingredients: " + ingredients.toString());
	}
}