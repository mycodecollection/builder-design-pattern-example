# Builder Design Pattern
## Description
Builder lets us construct complex objects step by step. It allows us to produce different types and representations of an object using the same construction code.

Builder is the best solution for constructing complex objects that have different types and representations. The way is to create a Builder Class that implements all the step to create the objects. We can also create a Director Class that defines the order to execute the building steps in order to completely hides the details of product construction from the client code.

## Example
The code example is below:

[Builder Example](https://gitlab.com/mycodecollection/builder-design-pattern-example/-/blob/main/BuilderPatternDemo.java)

I tried to implements the builder design in order to gain a better understanding. In this code, I use Pizza as an Complex Object. The output is to compare the result of using the builder, through the director, and without the builder.

## References
- [Refactoring Guru: Builder Design Pattern](https://refactoring.guru/design-patterns/builder)
- [Contoh Builder Design Pattern](https://ferry.vercel.app/blog/contoh-builder-design-pattern)
- [Creational Design Patterns: Builder Method](https://blog.airbrake.io/blog/design-patterns/builder-method)
- [How does Builder Design Pattern solves problems like URL creation?](https://www.youtube.com/watch?v=4ff_KZdvJn8)
